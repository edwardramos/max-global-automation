config = {
    "explicitWait": 1000,
    "implicitWait": 1000,
    "fluentWaitSec": 4000,
    "mochaTimeout": 20000,
    "todo": {
        "a": "1",
        "b": "2",
        "c": "3"
    },
}

module.exports = config
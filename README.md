# Global Automation Project

![NPM Version](https://img.shields.io/npm/v/npm)

This repository contains the source code for the Norwex Global Automation Test Suite for Max front end, back end, and services


## Usage
 -  install node, requires [v14.19.0](https://nodejs.org/download/release/latest-v14.x/)
 - `npm install`
 - `npm test`

> This will run the smoke tests for Max NA and AU

## Recommended
 - use IDE [VSCode](https://code.visualstudio.com/download)
 - install the VSCode extension Highlight (by Fabio Spampinato)

> VSCode > Extensions > Highlight > settings.json
```json
{
    "highlight.decorations": { "rangeBehavior": 3 },
    "highlight.regexes": {
        "(TODO)": [{ "color": "yellow" }], //edit to fit your local theme
        "(BUG)": [{ "color": "red" }],
        "(DEBUG)": [{ "color": "red" }]
    },
    "highlight.maxMatches": 1000
}
```


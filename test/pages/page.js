const webdriver = require('selenium-webdriver')
const { By, until } = require('selenium-webdriver')
var config = require('../../data/config')


function Page (_driver, url, title) {
    this.driver = _driver
    this.pageUrl = url
    this.pageTitle = title
}

Page.prototype.goto = function () {
    this.driver.get(this.pageUrl)
    // handleConnectionNotPrivateScreen()
    return this
}

Page.prototype.getPageTitle = function () {
    this.driver.getTitle().then(function (t) {
        return title
    })
}

Page.prototype.waitFor = function (locator, timeout) {
    var waitTimeout = timeout || 15000
    var driver = this.driver
    return driver.wait(until.elementLocated(locator, waitTimeout))
}

Page.prototype.handleConnectionNotPrivateScreen = function () {
    var title = driver.getTitle()
    console.log("title: " + title)
    if (title == 'Privacy error') {
        console.log('   bypassing connection not private screen...')
        var notPrivateAdvBtn = By.id('details-button')
        var notPrivateProceed = By.id('proceed-link')
        // this.waitFor(notPrivateAdvBtn)
        waitFor(notPrivateAdvBtn)
        driver.findElement(notPrivateAdvBtn).click()
        driver.findElement(notPrivateProceed).click()
    }
}

// NcoLoginPage.prototype.waitForDOMContentToLoad = async function () {
//     await console.log('waiting...')
//     let state = null
//     let timeout = 0
//     while (state == null && timeout < 10) {
//         try {
//             state = await js.executeScript('document.readyState;')
//             await this.driver.sleep(500)
//         } catch (err) {
//             await console.log(err.message)
//         }
//         await timeout++
//     }
// }

module.exports = Page
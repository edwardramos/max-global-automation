const webdriver = require('selenium-webdriver')
const { By, until } = require('selenium-webdriver')
var config = require("../../../data/config")
var page = require('../page')


function NcoDashboardPage (_driver) {
    page.call(this, _driver,
        'https://qa.biz/en_US/consultant/dashboard',
        'Norwex Consultant Office')
    this.welcomeMsg = By.css('.consultant-area h1')
    this.mainMenu_myDashboardBtn = By.css('.menu-wrapper section div.main-menu > li:nth-child(1) > a')
}

NcoDashboardPage.prototype = Object.create(page.prototype)
NcoDashboardPage.prototype.constructor = NcoDashboardPage


NcoDashboardPage.prototype.getWelcomeMsg = function () {
    return this.driver.findElement(this.welcomeMsg).getText()
}

NcoDashboardPage.prototype.dashboardBtn = function () {
    return this.driver.findElement(this.mainMenu_myDashboardBtn)
}

// NcoDashboardPage.prototype.clickSearchButton = function() {
//     this.driver.executeScript("document.getElementsByName('btnK')[0].click();")
//     this.waitFor({ id: 'search' })
//     return new NcoDashboardPage(this.driver)
// }


module.exports = NcoDashboardPage
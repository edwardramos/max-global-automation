const webdriver = require('selenium-webdriver')
const { By, until } = require('selenium-webdriver')
var config = require("../../../data/config")
var page = require('../page')


function NcoLoginPage (_driver) {
    page.call(this, _driver,
        'https://qa1.biz/en_US/consultant/login',
        'Login - Norwex Consultant Office')
    this.username = By.id('username')
    this.password = By.id('password')
    this.loginBtn = By.id('nco_login_submit')
    this.errorMsg = By.css('#cms > div.alert')
}

NcoLoginPage.prototype = Object.create(page.prototype)
NcoLoginPage.prototype.constructor = NcoLoginPage


NcoLoginPage.prototype.errorAlertMsg = function () {
    return this.driver.findElement(this.errorMsg)
}

NcoLoginPage.prototype.isCIDInputBoxPresent = function () {
    return this.driver.isElementPresent(this.username)
}

NcoLoginPage.prototype.enterCID = function (cid) {
    return this.driver.findElement(this.username).sendKeys(cid)
}

NcoLoginPage.prototype.enterPassword = function (passwrd) {
    return this.driver.findElement(this.password).sendKeys(passwrd)
}

NcoLoginPage.prototype.clickLoginBtn = function () {
    // this.driver.executeScript("arguments[0].click()", this.waitFor(By.css('#nco_login_submit')) )
    this.driver.findElement(this.loginBtn).click()
}

module.exports = NcoLoginPage
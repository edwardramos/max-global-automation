'use strict'

const webdriver = require('selenium-webdriver')
const { Builder, Capabilities, until } = require('selenium-webdriver')
webdriver.promise.USE_PROMISE_MANAGER = false

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

var config = require('../../../data/config')
var consultants = require('../../../data/testUserData')
var NcoLoginPage = require('../../pages/nco/login.page')
var NcoDashboardPage = require('../../pages/nco/dashboard.page')


describe('#Personal Supply Order - en_US', function () {

    this.timeout(config.mochaTimeout)

    var driver
    var ncoLoginPage
    var ncoDashboardPage

    beforeEach(async function () {
        let chromeCaps = Capabilities.chrome()
        chromeCaps.set('acceptInsecureCerts', true)
        driver = await new Builder().withCapabilities(chromeCaps).build()
        // await driver.manage().setTimeouts({ implicit: config.implicitWait })
        await console.log('driver running...')

        ncoLoginPage = await new NcoLoginPage(driver)
    })

    afterEach(async function () {
        try {
            await console.log('\tlog: shutting down driver...')
            await driver.quit()
        } catch (err) {
            await console.err(err.message)
        }
    })


    it('should deny access with wrong credentials', async function () {

        await ncoLoginPage.goto()
        await driver.sleep(1000)
        await console.log('entering fake credentials...')
        await ncoLoginPage.enterCID('foo')
        await ncoLoginPage.enterPassword('bar')
        await ncoLoginPage.clickLoginBtn()
        await driver.wait(until.elementIsVisible(ncoLoginPage.errorAlertMsg()))
        await driver
            .getTitle()
            .then(function (title) {
                console.log('on page:     ' + title)
                expect(title).to.contain('Login')
            })
        await driver
            .getCurrentUrl()
            .then(async function (url) {
                expect(url).to.not.contain('dashboard')
            })

    })

    it('should log in successfully to nco', async function () {

        //login

        // ncoLoginPage = await new NcoLoginPage(driver)
        await ncoLoginPage.goto()
        await driver.sleep(1000)
        await driver
            .getTitle()
            .then(function (title) {
                console.log('on page:     ' + title)
            })
        await ncoLoginPage.enterCID(consultants.en_US.username)
        await ncoLoginPage.enterPassword(consultants.en_US.password)
        await ncoLoginPage.clickLoginBtn()
        await driver.wait(until.urlContains('dashboard'))
        await driver
            .getTitle()
            .then(function (title) {
                console.log('on page:     ' + title)
            })
        await driver
            .getCurrentUrl()
            .then(async function (url) {
                expect(url).to.contain('dashboard')
            })

        //dashboard

        ncoDashboardPage = await new NcoDashboardPage(driver)
        await ncoDashboardPage
            .getWelcomeMsg()
            .then(function (msg) {
                console.log('welcome msg: ' + msg)
            })

    })


})

// const caps = require('../../caps')
const { Builder, By, Key } = require('selenium-webdriver')
var webdriver = require('selenium-webdriver')
var should = require('chai').should()

describe.skip('xray jenkins test', function() { 

    it('xray jenkins test', async function() {
        console.log('testing output 123')
        let x=1
        x.should.equal(1)
    })

})


describe.skip('xray jenkins test - firefox', function() {

    var driver
    
    beforeEach(function () {
        let firefoxCaps = webdriver.Capabilities.firefox()
        firefoxCaps.set("acceptInsecureCerts", true)
        driver = new Builder().withCapabilities(firefoxCaps).build()
    })

    afterEach(function () {
        driver.sleep(2000)
        driver.quit()
    })

    it('nco login - failing', async function () {

        await driver.get('https://qa.biz/en_US/consultant/login')
        await driver.sleep(1000)

        let loginFormHeader = await driver
            .findElement(By.css('#loginForm .consultant-login>legend'))
            .getText()
            .then(function (value) {
                return value
            })

        loginFormHeader.should.equal('this step should fail')

    })

})